import React, {useState, useEffect} from 'react'
import { connect } from 'react-redux'
import { Menu, Grid, Table, Image } from 'semantic-ui-react'
import { isEmpty, capitalize } from 'lodash'
import styled from 'styled-components'

import democratic from '../logos/dems.svg'
import republican from '../logos/reps.svg'

const Bold = styled.span`
  font-weight: bold;
`
const ElecName = styled.span`
  margin-left: 2rem;
`

const RepsSensInfo = ({reps, sens, ...rest}) => {
  const [elecOfficials, setElecOfficials] = useState([])
  const [active, setActive] = useState('')
  const [info, setInfo] = useState('')

  const onClick = (e, {name}) => {
    setActive(name)
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  const updateElecOfficials = () => {
    setElecOfficials([...reps, ...sens])
    setActive('')
    setInfo('')
  }
  useEffect(updateElecOfficials, [reps, sens])

  const updateInfo = () => {
    if(!isEmpty(active)){
      const elec = elecOfficials.find( elec => elec.name === active )
      setInfo(elec)
    }
  }
  useEffect(updateInfo, [active])

  const partyImageFor = (elec) => {
    if(elec.party === 'Republican'){
      return republican
    }
    return democratic
  }

  const textOrLink = (value) => {
    if(value.includes('http')){
      return ( <a href={value} target="_blank" rel="noopener noreferrer">Click</a> )
    }
    return value
  }

  const renderRepsSens = () => {
    if( !isEmpty(reps) || !isEmpty(sens) ){
      return elecOfficials.map((elec, index) => (
        <Menu.Item
          key={index}
          name={elec.name}
          active={elec.name === active}
          onClick={onClick}
        >
          <Image size='mini' src={partyImageFor(elec)} avatar />
          <ElecName>{elec.name}</ElecName>
        </Menu.Item>
      ))
    }
  }

  const renderElecInfo = () => {
    if(!isEmpty(info)){
      return (
        <Table basic='very'>
          <Table.Body>
            {Object.entries(info).map(([key, val], index) =>(
              <Table.Row key={index}>
                <Table.Cell>
                  <Bold>
                    {capitalize(key)}
                  </Bold>
                </Table.Cell>
                <Table.Cell>{textOrLink(val)}</Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      )
    }
  }

  return (
    <Grid.Row columns={2}>
      <Grid.Column width={5}>
        <Menu pointing secondary vertical style={{ width: '100%' }}>
          {renderRepsSens()}
        </Menu>
      </Grid.Column>
      <Grid.Column width={11}>
        {renderElecInfo()}
      </Grid.Column>
    </Grid.Row>
  )
}

const mapStateToProps = (state, props) => {
  return {
    reps: state.reps_sens.reps,
    sens: state.reps_sens.sens,
  }
}

export default connect(mapStateToProps)(RepsSensInfo)
