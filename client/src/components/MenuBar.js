import React, {useState, useEffect} from 'react'
import { connect } from 'react-redux'
import { Menu, Form } from 'semantic-ui-react'
import { indexReps, indexSens, indexRepsSens } from '../reducers/reps_sens'
import { states as USStates } from '../data/states'


const optionsForRepsSens = [
  { key: 'Representatives', text: 'Representatives', value: 'representatives' },
  { key: 'Senators', text: 'Senators', value: 'senators' },
  { key: 'Either', text:'Either', value: 'either' }
]

const optionsForStates = USStates.map(usState => ({
  key: usState.abbreviation, 
  text: usState.name,
  value: usState.abbreviation,
}))

const styles = {
  select: {
    width: '20rem',
  },
}

const defaults = {
  selectedType: 'either',
  selectedState: 'UT',
}

const MenuBar = ({dispatch, ...rest}) => {
  const [state, setState] = useState({...defaults})

  const runQuery = () => {
    const { selectedType, selectedState } = state
    if(selectedType && selectedState){
      if(selectedType === 'representatives'){
        dispatch(indexReps(selectedState))
      } else if(selectedType === 'senators'){
        dispatch(indexSens(selectedState))
      } else if(selectedType === 'either'){
        dispatch(indexRepsSens(selectedState))
      }
    }
  }
  useEffect(runQuery, [state.selectedType, state.selectedState])

  const onChange = (e, {name, value}) => {
    setState(state => ({
      ...state,
      [name]: value
    }))
  }

  const renderRepsSens = () => (
    <Form.Field>
      <label>Affiliation</label>
      <Form.Select 
        style={styles.select}
        placeholder='Select affiliation...'
        name='selectedType'
        value={state.selectedType}
        onChange={onChange}
        options={optionsForRepsSens}
      />
    </Form.Field>
  )

  const renderStates = () => (
    <Form.Field>
      <label>State</label>
      <Form.Select 
        style={styles.select}
        placeholder='Select a State...'
        name='selectedState'
        value={state.selectedState}
        onChange={onChange}
        options={optionsForStates}
      />
    </Form.Field>
  )

  return (
    <Menu secondary>
      <Form>
        <Form.Group>
          {renderRepsSens()}
          {renderStates()}
        </Form.Group>
      </Form>
    </Menu>
  )
}

export default connect()(MenuBar)
