import React, {useEffect} from 'react'
import { connect } from 'react-redux'
import { Grid, Segment, Header, Image } from 'semantic-ui-react'
import { indexRepsSens } from '../reducers/reps_sens'
import styled from 'styled-components'

import congress from '../logos/congress.svg'

import MenuBar from './MenuBar'
import RepsSensInfo from './RepsSensInfo'

const Layout = styled.div`
  width: 70%;
  margin-top: 5rem;
  margin-left: 15%;
  margin-right: 15%;
`
const Centered = styled.div`
  display: flex;
  justify-content: center;
`

const RepsSensApp = ({dispatch, ...rest}) => {

  const loadRepsAndSens = () => {
    dispatch(indexRepsSens()) // initial defaults
  }
  useEffect(loadRepsAndSens, []) // On Mount

  return (
    <Layout>
      <Header as='h2' icon textAlign='center'>
        <Image src={congress} circular style={{ width: '200px', height: '200px' }}/>
        <Header.Content>United States Congress</Header.Content>
        <Header.Subheader>
          Welcome to a listing of all the men and women that serve as a member of the United
          States Congress. Please use the menus below to filter between the members that are part
          of the House of Representatives and Senate. You may also further filter the members
          by filtering by a state. Once your desired filters are set, click on the name of the
          serving individual to see information relating to their service. Enjoy!
        </Header.Subheader>
      </Header>
      <Segment>
        <Grid>
          <Grid.Row columns={1}>
            <Grid.Column width={16}>
              <Centered>
                <MenuBar />
              </Centered>
            </Grid.Column>
          </Grid.Row>
          <RepsSensInfo />
        </Grid>
      </Segment>
    </Layout>
  )
}

export default connect()(RepsSensApp)
