import React from 'react';
import { Switch, Route } from 'react-router-dom'

import RepsSensApp from './components/RepsSensApp'

function App() {
  return (
    <>
      <Switch>
        <Route exact path='/' component={RepsSensApp} />
        <Route component={RepsSensApp} />
      </Switch>
    </>
  );
}

export default App;
