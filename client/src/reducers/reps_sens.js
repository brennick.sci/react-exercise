import createReducer from './createReducer'
import axios from 'axios'

// Constants
const INDEX_REPS = 'INDEX_REPS'
const INDEX_SENS = 'INDEX_SENS'
const UPDATE_SELECTED = 'UPDATE_SELECTED'

// Actions
export const indexReps = (state = 'UT', only = true) => {
  return dispatch => {
    axios.get(`/representatives/${state}`)
      .then(res => {
        dispatch({
          type: INDEX_REPS,
          reps: res.data.success ? res.data.results : [],
          only,
        })
      })
  }
}

export const indexSens = (state = 'UT', only = true) => {
  return dispatch => {
    axios.get(`/senators/${state}`)
      .then(res => {
        dispatch({
          type: INDEX_SENS,
          sens: res.data.success ? res.data.results : [],
          only,
        })
      })
  }
}

export const indexRepsSens = (state = 'UT', only = false) => {
  return dispatch => {
    dispatch(indexReps(state, only))
    dispatch(indexSens(state, only))
  }
}

// Reducers
const indexRepsReducer = (state, actions) => {
  if(actions.only){
    return {...state, reps: actions.reps, sens: []}
  } else {
    return {...state, reps: actions.reps}
  }
}

const indexSensReducer = (state, actions) => {
  if(actions.only){
    return {...state, sens: actions.sens, reps: []}
  } else {
    return {...state, sens: actions.sens}
  }
}

// Initial State
const defaults = {
  reps: [],
  sens: [],
}

// Reudcer
export const reps_sens = createReducer({...defaults}, {
  [INDEX_REPS]: indexRepsReducer,
  [INDEX_SENS]: indexSensReducer,
})
