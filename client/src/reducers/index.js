import { combineReducers } from 'redux'

// Composed Reducers
import { reps_sens } from './reps_sens'

const rootReducer = combineReducers({
  reps_sens,
})

export default rootReducer
